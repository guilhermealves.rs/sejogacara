<?php
$banner = get_field('imagem_destaque_do_slide', $post->ID);
$img = get_the_post_thumbnail_url();
get_header(); ?>
<div class="<?php global $post; echo $post->post_name;?> interna">
	<div id="postBlog" class="sessao">
		<div class="imgDestaque" style="background: url('<?php if (!empty($banner)): echo $banner; else: echo $img; endif; ?>')">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="box">
				<h1> <?php the_title(); ?> </h1>
				<div class="info">
					<div class="data">
						<?php the_date('d F Y') ?>
					</div>
					<span>publicado por:</span>
					<div class="autor">
					  	<?php echo get_avatar( get_the_author_meta( 'ID' ), 64 ); ?>

						<h3><?php the_author() ?></h3>

					</div>
				</div>
			<div class="container">
				<div class="breadcrumbs"><?php echo breadcrumbs() ?></div>
			</div>
			</div>
		</div>
		 <div id="conteudo">
	        <div class="container">
	        	
	            <div id="post" class="sessao">
					<div class="conteudo"><?php echo the_content(); ?></div>
					
					<div id="relacionados" class="sessao">
						<?php get_template_part('template/sessao', 'relacionados') ?>
					</div>

	            </div>

	            <!-- Sidebar -->

	            <div id="sidebar" class="sessao">
	                <?php get_template_part('template/sessao', 'sidebar') ?>
	            </div>
	        </div>
	    </div>

	<?php endwhile; endif; ?>
	</div>




</div>

<?php
get_footer();