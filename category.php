<?php
$category = get_queried_object();
$categoria = $category->term_id;
$banner_categoria = get_field('banner_categoria', $category);
$icone_categoria = get_field('icone_categoria', $category);



get_header(); ?>
<div class="<?php global $post; echo $post->post_name;?> interna">
	<div id="postBlog" class="sessao">
		<div class="imgDestaque" style="background: url('<?php if (!empty($banner_categoria)): echo $banner_categoria; else: echo $icone_categoria; endif; ?>')">
			<div class="box">
				<h1> <?php single_cat_title(); ?> </h1>
			<div class="container">
				<div class="breadcrumbs"><?php echo breadcrumbs() ?></div>
			</div>
			</div>
			
		</div>
		 <div id="conteudo">
	        <div class="container">
	        	
	            <div class="conteudoBlog">
	            	<?php 
	            	query_posts('cat='. $category->term_id); ?>
	            	<?php if ( have_posts() ) :	while ( have_posts() ) : the_post(); ?>
						<?php get_template_part('template/conteudo', 'blog') ?>
					<?php endwhile; else : 
					echo '<div class="sem-post">';
					echo 'Nenhum artigo encontrado nesta categoria. Voltar para a <a href="'. get_home_url(). '">&nbsp; página inicial</a>';
					echo '</div>';
					endif; ?>
	            </div>

	            <!-- Sidebar -->

	            <div id="sidebar" class="sessao">
	                <?php get_template_part('template/sessao', 'sidebar') ?>
	            </div>
	        </div>
	    </div>

	</div>




</div>

 
<?php
get_footer();