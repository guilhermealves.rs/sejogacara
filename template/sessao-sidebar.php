<div class="boxSidebar">

	<div class="busca">
		<input type="text">
		<div class="logo">
			<img src="<? echo get_template_directory_uri()?>/assets/img/logoFull.png" alt="">
		</div>
	</div>	
	
	<div class="categorias">
		<div class="heading">
			<h3>Categorias</h3>
			<ul>
		    	<?php $categoriasSidebar = array ('title_li' => '', 'hide_empty' => '0', 'orderby' => 'name');
		    	wp_list_categories($categoriasSidebar); ?> 
			</ul>
		</div>
	</div>

	<div class="autor">
		<img src="<? echo get_template_directory_uri()?>/assets/img/perfil.jpg" alt="">
		<h3>Edu Cassau</h3>
		<div class="desc">
			Explorador do mundo e de mim mesmo,
			nômade digital desde 2014, percorri a Austrália, Tailândia, Índia, Nepal e Nova Zelândia, até encontrar a ter clareza dos meus objetivos, visões e propósito.
		</div>
	</div>

	<div class="vem-e-se-joga">
		<div class="heading">
			<h3>
				Vem e se joga
			</h3>
		</div>
		<div class="desc">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste aliquid quis architecto vitae delectus porro.
		</div>
		<form action="">
			<input type="text" placeholder="Seu nome">
			<input type="email" placeholder="Seu email">
			<input type="submit" value="Quero me jogar">
		</form>
	</div>

	<div class="facebook">
		<div class="heading">
			<h3>
				Facebook
			</h3>
			<div class="pagina">
				<a href="https://www.facebook.com/sejogacara/" alt="Chega aí, e confere minha jornada!" title="Chega aí, e confere minha jornada!" target="_new"><img src="<? echo get_template_directory_uri()?>/assets/img/sidebarFacebook.jpg" alt=""></a>
				<a class="link" target="_new" href="https://www.facebook.com/sejogacara/">Visite nossa página</a>
			</div>
		</div>
	</div>


	<div class="instagram">
		<div class="heading">
			<h3>
				Instagram
			</h3>
		</div>
		<div class="boxInstagram">
	
<div id="instafeed">
	
</div>
		</div>
	</div>

	<div class="youtube">
		<div class="heading">
			<h3>
				Youtube
			</h3>
		</div>
		
		

		<div class="boxYoutube">

			<div id="static_video"></div>
    <script type="text/javascript">
        var channelID = "UCDn5_Ab9_Ubu52P6HyEx3lA";
			$.getJSON('https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.youtube.com%2Ffeeds%2Fvideos.xml%3Fchannel_id%3D'+channelID, function(data) {
			   var link = data.items[0].link;
			   var id = link.substr(link.indexOf("=")+1);
			    $("#youtube_video img").attr('src', 'https://img.youtube.com/vi/' +id+ '/hqdefault.jpg');
			    $("#yt-video").attr('data-yt', '' +id+ '');
			});
    </script>
   <div id="youtube_video">

   <a id="yt-video" href="#yt-video" rel="modal" data-yt=""><img src="" alt=""></a>
   </div>
		</div>
	</div>
</div>

<script>
		jQuery(function($){

			var attr = $("#yt-video").attr("data-yt");

		

		$("#yt-video").click(function(){
			console.log('esse é um id válido: ' + attr + ' ?');
			var video = $("#video ").attr('src', 'http://www.youtube.com/embed/'+ attr +'?rel=0');
			var idVideo = $("#video ").attr('src');
			$("#video > iframe").attr('src', 'https://www.youtube.com/embed/'+attr);
			$("#video > iframe").hide();
			console.log('atributo correto: ' + video + ' ? .. e o id do video é esse link? : ' + idVideo + ' :)');
			$('.window').show('slow');
			$("#video > iframe").delay(850).show("slow");

		})
	})
</script>