<div class="destaque">
	<div class="heading">
		<h3>
			Viagem
		</h3>
		<span>Lorem ipsum dolor sit amet, consectetur <a href="#"> adipisicing </a> elit.</span>
	</div>
<!-- ############# Destaques ############### -->
<?php 
$args = array( 'posts_per_page'	=> '1',	'post_type' => 'post',	'meta_query' => array( array( 'key' => 'principal', 'compare' => '==', 'value' => '1' )	) ); $the_query = new WP_Query( $args ); ?>
		<?php if( $the_query->have_posts() ): ?>
			<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
<a href="<?php the_permalink() ?>">
	<div class="boxDestaque" style="background: url('<? echo the_post_thumbnail_url( 'medium-large' ) ?>')">
		<div class="conteudo">
			<div class="desc">
				<div class="data"><?php echo 'Enviado à ' . human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' atrás'; ?></div>
				<h1><? the_title() ?></h1>
				<div class="tags">
					<?php 
					$posttags = get_the_tags();
					$count=0; $sep='';
					if ($posttags) {
						echo '<ul>';
						foreach($posttags as $tag) {
							$count++;
							echo '<li><a title="'.$tag->name.'" href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a></li>';
							if( $count > 5 ) break; //change the number to adjust the count
						}
						echo '</ul>';
					} ?>
				</div>
			</div>
			<div class="share">
<img class="share-icon" src="<? echo get_template_directory_uri(); ?>/assets/img/share.png" alt="">
				<div class="share_btn">
					<a href="http://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo urlencode(the_title()); ?>&p[url]=<?php echo urlencode(get_permalink()); ?>&p[summary]=<?php echo urlencode(the_excerpt()); ?>" target="popup" onclick="window.open('http://www.facebook.com/sharer/sharer.php?s=100&p[url]=http://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo urlencode(the_title()); ?>&p[url]=<?php echo urlencode(get_permalink()); ?>&p[summary]=<?php echo urlencode(the_excerpt()); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-facebook" aria-hidden="true"></i></a>

				<a href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>+<?php echo get_permalink(); ?>" target="popup" onclick="window.open('https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>+<?php echo get_permalink(); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-twitter" aria-hidden="true"></i></a>

				<a href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>" target="popup" onclick="window.open('https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
				</div>

				
			</div>
		</div>
	</div>
</a>
	<?php endwhile; ?>
	<?php endif; ?>
<?php wp_reset_query(); ?>
</div>
<!-- ############# Principais ############### -->
<div class="principais">
	<div class="heading">
		<h3>
			Viagem
		</h3>
	</div>
	<div class="boxPrincipais">
		<?php 
		$args = array( 'posts_per_page'	=> '3',	'post_type' => 'post', 'meta_query' => array( array( 'key' => 'destaque', 'compare' => '==', 'value' => '1' )	) ); $the_query = new WP_Query( $args ); ?>
					<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="cardPost">
			<header>
				<div class="data"><?php $post = get_post(); echo get_the_date(' d <b> M </b>', $post->ID  ); ?> </div>
				<div class="img">
					<img src="<? echo the_post_thumbnail_url( 'medium-large' ) ?>" alt="">
				</div>
				
			</header>
			<footer>
				<h1><? the_title() ?></h1>
				<div class="conteudo">
					<? the_excerpt() ?>
				</div>
			</footer>
			<div class="verMais">
				<div class="share">
					<img class="share-icon" src="<? echo get_template_directory_uri(); ?>/assets/img/share.png" alt="">
					<div class="share_btn">
					<a href="http://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo urlencode(the_title()); ?>&p[url]=<?php echo urlencode(get_permalink()); ?>&p[summary]=<?php echo urlencode(the_excerpt()); ?>" target="popup" onclick="window.open('http://www.facebook.com/sharer/sharer.php?s=100&p[url]=http://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo urlencode(the_title()); ?>&p[url]=<?php echo urlencode(get_permalink()); ?>&p[summary]=<?php echo urlencode(the_excerpt()); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-facebook" aria-hidden="true"></i></a>

				<a href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>+<?php echo get_permalink(); ?>" target="popup" onclick="window.open('https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>+<?php echo get_permalink(); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-twitter" aria-hidden="true"></i></a>

				<a href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>" target="popup" onclick="window.open('https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
				</div>

				
				</div>
				<div class="continueLendo">
					<a href="<? the_permalink() ?>">Continue Lendo</a>
				</div>
			</div>
		</div>
				<?php endwhile; ?>
		<?php wp_reset_query(); wp_reset_postdata();?>
	</div>
</div>
<!-- ############# Blog ############### -->
<div class="conteudoBlog">
	<?php 
	$args = array( 'posts_per_page'	=> '4',	'post_type' => 'post', ); $the_query = new WP_Query( $args ); ?>
		<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php get_template_part('template/conteudo', 'blog') ?>
		<?php endwhile; ?>
	<?php wp_reset_query(); wp_reset_postdata();?>
</div>