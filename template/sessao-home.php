<div id="slide_home">
<?php 
    $contador = 0;
	$args = array( 'posts_per_page'	=> '-1', 'post_type' => 'post' ); $the_query = new WP_Query( $args ); ?>
    <?php while( $the_query->have_posts() ) : $the_query->the_post(); 

    $banner = get_field('imagem_destaque_do_slide', $post->ID);
    $img = get_the_post_thumbnail_url();
    
    if( get_field('slide') ):
        if($contador < 5):?>
    <div> 
        <div class="boxSlide" style="background: url('<?php if (!empty($banner)): echo $banner; else: echo $img; endif; ?>')">
            <div class="conteudoSlide">
                <div class="slideInfo">
                    <div class="container">
                        <div class="desc">
                            <div class="tags">
                            <?php 
                                $posttags = get_the_tags();
                                $count=0; $sep='';
                                if ($posttags) {
                                    echo '<ul>';
                                    foreach($posttags as $tag) {
                                        $count++;
                                        echo '<li><a title="'.$tag->name.'" href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a></li>';
                                        if( $count > 5 ) break; //change the number to adjust the count
                                    }
                                echo '</ul>';
                            } ?>
                            </div>
                            <h1><?php the_title() ?></h1>
                            <div class="data"><?php echo 'Enviado à ' . human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' atrás'; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $contador++; endif; endif; endwhile; ?>
	<?php wp_reset_query(); wp_reset_postdata();?>
</div>
<div class="menuDestaque">
    <div class="container">
        <ul>
            <?php 
            $terms = get_terms( array(
                'taxonomy' => 'category',
                'hide_empty' => false,
            ) );
            $count=0;
            if ($terms) {
                foreach($terms as $cat) {
                    $count++;
                 
                    $img = get_field('icone_categoria', $cat);
                    echo 
                    '<li>'.
                        '<a title="'.$cat->name.'" href="'.get_term_link($cat->term_id).'">'.
                            '<div class="item" style="background:url('.$img.');">'.
                                '<h2><span>'.$cat->name.'</span></h2>'.
                            '</div>'.
                        '</a>'.
                    '</li>';
                    if( $count > 5 ) break; //change the number to adjust the count
                }
            }
            ?>
        </ul>
    </div>
</div>