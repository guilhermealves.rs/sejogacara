<div class="cardBlog">
    <div class="titulo">
        <h1><?php the_title() ?></h1>
    </div>
    <div class="conteudo">
        <div class="img">
            <?php $thumb = get_the_post_thumbnail_url(); 
            if(!empty($thumb)): ?>
                <img src="<?php echo $thumb; ?>" alt="">
            <?php else: ?>
                <img src="<?php echo get_template_directory_uri() . '/assets/img/semimagem.png' ?>" alt="">
            <?php endif; ?>
        </div>
        <div class="desc">
            <?php the_excerpt(); ?>
            
        </div>
    </div>
    <div class="tags">
        <ul>
        <?php
            $posttags = get_the_tags();
            $count=0;;
            if ($posttags) {
                foreach($posttags as $tag) {
                    $count++;
                    echo '<li>' . '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>' . '</li>';
                    if( $count > 2 ) break;
                }
            }
        ?>
        </ul>
        <div class="btn"><a class="leiaMais" href="<? the_permalink(); ?>">Leia mais</a></div>
    </div>
</div>