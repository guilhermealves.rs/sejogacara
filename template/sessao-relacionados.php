<div class="heading">
	<h3>Leia também</h3>
</div>

<div class="boxRelacionados">
	<?php 
	$args = array( 'posts_per_page'	=> '3',	'post_type' => 'post' ); $the_query = new WP_Query( $args ); ?>
			<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<div class="item">
		<div class="img" style="background: url('<?php echo the_post_thumbnail_url('medium-large') ?>')">
		</div>
		<div class="info">
			<a href="<?php the_permalink() ?>"><h2><?php the_title() ?></h2></a>
			<h6><?php echo 'Enviado à ' . human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' atrás'; ?></h6>
			<div class="chamada">
				<?php echo excerpt(20)?>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</div>