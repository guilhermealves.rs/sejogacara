<?php
get_header(); ?>
    <div id="home">
        <?php get_template_part('template/sessao', 'home') ?>
    </div>

    <!-- Sessão 1 - ebook -->

    <div id="ebook" class="sessao">
        <?php get_template_part('template/sessao', 'ebook') ?>
    </div>

    <!-- Sessão 2 - Blog -->
    <div id="conteudo">
        <div class="container">
            <div id="blog" class="sessao">
                <?php get_template_part('template/sessao', 'blog') ?>
            </div>

            <!-- Sessão 3 - Sidebar -->

            <div id="sidebar" class="sessao">
                <?php get_template_part('template/sessao', 'sidebar') ?>
            </div>
        </div>
    </div>

<?php
get_footer();