<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<?php wp_head(); ?>
<link rel="icon" href="<?php echo get_template_directory_uri();?>/img/favicon.png" type="image/x-icon" />
<script type='text/javascript' src='<?php echo get_template_directory_uri()?>/js/instafeed.js'></script>

<?php
        // General Variables
        $mtitle_default = get_bloginfo('name');
        $title_default  = get_bloginfo('name');
        $home_default   = get_home_url();
       
        $text = get_option('general_opts');
        $text = $text['tags'];

        $keys_default   = $text;
        $link_default   = get_home_url();
        $desc_default   = get_bloginfo('description');
        $image_default  = get_template_directory_uri() . '/assets/img/sejogacara.png';
        if (is_single() || is_page()) {
            $title_default = get_the_title($post->ID);
            $link_default  = get_permalink();
            if(has_post_thumbnail()){
                $image_ID      = get_post_thumbnail_id(get_the_id());
                $image_default = wp_get_attachment_image_src($image_ID, 'full');
                $image_default = $image_default[0];
            } else {
                $image_default = get_template_directory_uri() . '/assets/img/sejogacara.png';
            }
        }

        global $post;
        if (!is_404()) {
          $posttags = wp_get_post_tags( $post->ID );
        }
?>

     <!-- Código do Schema.org também para o Google+ -->
    <meta itemprop="name" content="<?php echo $title_default; ?>">
    <meta itemprop="description" content="<?php echo $desc_default; ?>">
    <meta itemprop="image" content="<?php echo $image_default; ?>">

    <!-- Open Graph Meta Data -->
    
    <!-- <meta name="p:domain_verify" content=""/> -->
    <meta property="og:title" content="<?php echo $title_default; ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo get_home_url();?>"/>
    <meta property="og:image" content="<?php echo $image_default; ?>"/>

    <!-- Twitter Card -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@sejogacara">
    <meta name="twitter:title" content="<?php echo $title_default; ?>">
    <meta name="twitter:description" content="<?php echo $desc_default; ?>">
    <meta name="twitter:creator" content="@sejogacara">
    <meta name="twitter:image" content="<?php echo $image_default; ?>">
    <meta itemprop="name" content="<?php echo $title_default; ?>">
    <meta itemprop="description" content="<?php echo $desc_default; ?>">
    <meta itemprop="image" content="<?php echo $image_default; ?>">

    <!-- Google Geo Location Meta Data -->
   <!--  <meta name="geo.region" content="" />
    <meta name="geo.placename" content="" />
    <meta name="geo.position" content="" />
    <meta name="ICBM" content="" /> -->

    <!-- Dublin Core Meta Data -->
    <meta name="dc.language" content="PT-BR">
    <meta name="dc.creator" content="<?php echo $mtitle_default; ?>">
    <meta name="dc.publisher" content="<?php echo $mtitle_default; ?>">
    <meta name="dc.source" content="<?php echo $home_default; ?>">
    <meta name="dc.relation" content="<?php echo $link_default; ?>">
    <meta name="dc.title" content="<?php echo $title_default; ?>">
    <meta name="dc.keywords" content="<?php echo $keys_default; ?>, <?php if($posttags){foreach($posttags as $tag){echo $tag->name . ', ';}}; ?>">
    <meta name="dc.subject" content="<?php echo $title_default; ?>">
    <meta name="dc.description" content="<?php echo $desc_default; ?>">


<body <?php body_class(); ?>>
<div id="pageloader">
	<div class="sejoga"><img src="<? echo get_template_directory_uri();?>/assets/img/logoLoad.png" alt=""></div>
</div>
<header id="topo">
	<div class="container">
	    <div class="logo">
	        <a href="<?php echo get_home_url() ?>"><img src="<? echo get_template_directory_uri();?>/assets/img/logo.png" alt=""></a>
	    </div>
	    <nav>
	        <ul>
	            <li>a estrada</li>
	            <li>as descobertas</li>
	            <li>a liberdade</li>
	            <li>primeiro mochilão?</li>
	            <li>sobre</li>
	            <li>contato</li>
	        </ul>
	    </nav>
    </div>
</header>