<?php wp_footer(); ?>


<!-- ############# News ############## -->

<div id="news" class="sessao">
    <?php get_template_part('template/sessao', 'news') ?>
</div>



<!-- ############# Rodapé ############## -->

<footer id="principal">
    <div class="container">
        <div id="rodape">
    <!-- ############# Sobre ############### -->
            <div class="sobre">
                <div class="box">
                    <div class="img">
                        <img src="<? echo get_template_directory_uri() ?>/assets/img/logo-footer.png" alt="">
                    </div>
                    <div class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque consequuntur totam nam in, perferendis dicta debitis, illo atque tenetur incidunt vitae facilis reiciendis id ad culpa ea velit numquam odit!
                    </div>
                    <div class="social">
                        <ul>
                            <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
    <!-- ############# Menu ############### -->
            <div class="menu">
                <div class="box">
                    <div class="heading">
                        <h3>Menu Rápido</h3>
                    </div>
                    <ul>
                        <li>Ebook Mochilão</li>
                        <li>Viagens</li>
                        <li>Autoconhecimento</li>
                        <li>Nomadismo Digital</li>
                    </ul>
                </div>
            </div>
    <!-- ############# Dicas ############### -->
            <div class="dicas">
                <div class="box">
                    <div class="heading">
                        <h3>Dicas</h3>
                    </div>
                     <ul>
                        <li>Ebook Mochilão</li>
                        <li>Viagens</li>
                        <li>Autoconhecimento</li>
                        <li>Nomadismo Digital</li>
                    </ul>
                </div>
            </div>
    <!-- ############# Parceiros ############### -->
            <div class="parceiros">
                <div class="box">
                    <div class="heading">
                        <h3>Parceiros</h3>
                    </div>
                    <div class="lstParceiros">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                    </div>
                </div>
            </div>
        </div>
    <!-- ############# Copyright ############### -->  
        <div id="copyright">
            Todos os direitos reservados a cadu cassau, Se joga Cara!
        </div>
    </div>
</footer>
<div id="mascara"></div>
<div class="window" id="video">
    <div class="load" style="position: absolute;
    justify-content: center;
    align-items: center;
    display: flex;
    width: 100%;
    height: 100%;
    z-index: -1;">carregando...</div>
  <iframe id="frame" width="560" height="315" src="" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
<!-- ############# Scripts ############### -->

<script>
    jQuery(function($){
        $('#slide_home').slick({
            arrows: false
        });
    })
</script>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:400,500,700" rel="stylesheet">

<!-- ######### DEFFER IMG ######### -->
<script>
    function init() {
        var imgDefer = document.getElementsByTagName('img');
            for (var i=0; i<imgDefer.length; i++) {
            if(imgDefer[i].getAttribute('data-src')) {
            imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
            } 
        }    
    }
    window.onload = init;
</script>

<script>
    jQuery(function($){
    $('.share_btn').hide();
       $('.share-icon').toggle(function(){
         console.log('clico no bagulinho');
        $('.share_btn').hide();
        $('.share-icon').removeClass('ativo');
        $(this).addClass('ativo');
        $(this).next('.share_btn').slideDown('slow');;
    }, function() {
        $(this).next('.share_btn').slideUp("slow");
        $(this).removeClass('ativo');
    })
       
       }) 
    
</script>
<script>
    jQuery(function($){
        $(document).ready(function(){
        $("a[rel=modal]").click( function(ev){
        ev.preventDefault();
 
        var id = $(this).attr("href");
 
        var alturaTela = $(document).height();
        var larguraTela = $(window).width();
     
        //colocando o fundo preto
        $('#mascara').css({'width':larguraTela,'height':alturaTela});
        $('#mascara').fadeIn(1000); 
        $('#mascara').fadeTo("slow",0.8);
 
        var left = ($(window).width() /2) - ( $(id).width() / 2 );
        var top = ($(window).height() / 2) - ( $(id).height() / 2 );
     
        $(id).css({'top':top,'left':left});
        $(id).show();   
    });
 
    $("#mascara").click( function(){
        $(this).hide();
        $(".window").hide();
          console.log('fecha');
          $("#frame").attr('src', ' ');
    });
 
    $('.fechar').click(function(ev){
        ev.preventDefault();
        $("#mascara").hide();
        $(".window").hide();
        console.log('fecha');
        $("#frame").attr('src', ' ');
    });
});
    })
</script>



<script>
    jQuery(function($){
        $(window).on('scroll', function() {
            scrollPosition = $(this).scrollTop();
            if (scrollPosition >= 620) {
                // If the function is only supposed to fire once
                $(".logo").addClass('anima');
                // Other function stuff here...
            }
            else if(scrollPosition <= 620 ) {
                $(".logo").removeClass('anima');
            }
        });
    })
</script>
        <script type="text/javascript">
                jQuery(function(){
    var feed = new Instafeed({
        get: 'user',
         userId: '228437960',
         limit: '6',
         accessToken: '1299566080.3a81a9f.aecbe7d3a3ad45e0a1d8c9e632afe96b',
         template: '<div class="item"><a target="_new" href="{{link}}"><img src="{{image}}" /></a></div>'
    });
    feed.run();
    })
</script>
<script>
    jQuery(function($){
        var x = document.referrer;
        console.log('a var é:' + x)
    })
</script>
<script>
    jQuery(function(){
    $(document).ready(function() {
        $(window).load(function() {
             $('#pageloader').css('opacity', '0').delay(350).hide(0);
             $('body').css('overflow', 'auto');
        });
    });
})
</script>

<!-- ############# Facebook Page Plugin ############### -->

</body>
</html>
