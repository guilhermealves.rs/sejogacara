<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet (lowercase and without spaces)
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);
	// echo $themename;
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __('One', 'options_check'),
		'two' => __('Two', 'options_check'),
		'three' => __('Three', 'options_check'),
		'four' => __('Four', 'options_check'),
		'five' => __('Five', 'options_check')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __('French Toast', 'options_check'),
		'two' => __('Pancake', 'options_check'),
		'three' => __('Omelette', 'options_check'),
		'four' => __('Crepe', 'options_check'),
		'five' => __('Waffle', 'options_check')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();
	// ------- Sessão Slide ---------- //
	$options[] = array(
		'name' => __('Categorias da Home', 'options_check'),
		'type' => 'heading');

	$options[] = array(
		'desc' => __('Cards das categorias a serem exibidas na home.', 'options_check'),
		);

/* ############### Categoria 01 ################ */

	$options[] = array(
		'name' => __('Categoria 01', 'options_check'),
		'desc' => 'Imagem da categoria',
		'id' => 'cat01',
		'type' => 'upload',);
	$options[] = array(
		'desc' => __('Nome da categoria', 'options_check'),
		'id' => 'cat01_nome',
		'type' => 'text',);
	$options[] = array(
		'desc' => __('Link da categoria', 'options_check'),
		'id' => 'cat01_link',
		'type' => 'text',);

/* ############### Categoria 02 ################ */

	$options[] = array(
		'name' => __('Categoria 02', 'options_check'),
		'desc' => 'Imagem da categoria',
		'id' => 'cat02',
		'type' => 'upload',);
	$options[] = array(
		'desc' => __('Nome da categoria', 'options_check'),
		'id' => 'cat02_nome',
		'type' => 'text',);
	$options[] = array(
		'desc' => __('Link da categoria', 'options_check'),
		'id' => 'cat02_link',
		'type' => 'text',);

/* ############### Categoria 03 ################ */

	$options[] = array(
		'name' => __('Categoria 03', 'options_check'),
		'desc' => 'Imagem da categoria',
		'id' => 'cat03',
		'type' => 'upload',);
	$options[] = array(
		'desc' => __('Nome da categoria', 'options_check'),
		'id' => 'cat03_nome',
		'type' => 'text',);
	$options[] = array(
		'desc' => __('Link da categoria', 'options_check'),
		'id' => 'cat03_link',
		'type' => 'text',);

/* ############### Categoria 04 ################ */

	$options[] = array(
		'name' => __('Categoria 04', 'options_check'),
		'desc' => 'Imagem da categoria',
		'id' => 'cat04',
		'type' => 'upload',);
	$options[] = array(
		'desc' => __('Nome da categoria', 'options_check'),
		'id' => 'cat04_nome',
		'type' => 'text',);
	$options[] = array(
		'desc' => __('Link da categoria', 'options_check'),
		'id' => 'cat04_link',
		'type' => 'text',);

/* ############### Categoria 05 ################ */

	$options[] = array(
		'name' => __('Categoria 05', 'options_check'),
		'desc' => 'Imagem da categoria',
		'id' => 'cat05',
		'type' => 'upload',);
	$options[] = array(
		'desc' => __('Nome da categoria', 'options_check'),
		'id' => 'cat05_nome',
		'type' => 'text',);
	$options[] = array(
		'desc' => __('Link da categoria', 'options_check'),
		'id' => 'cat05_link',
		'type' => 'text',);

/* ############### Categoria 06 ################ */

	$options[] = array(
		'name' => __('Categoria 06', 'options_check'),
		'desc' => 'Imagem da categoria',
		'id' => 'cat06',
		'type' => 'upload',);
	$options[] = array(
		'desc' => __('Nome da categoria', 'options_check'),
		'id' => 'cat06_nome',
		'type' => 'text',);
	$options[] = array(
		'desc' => __('Link da categoria', 'options_check'),
		'id' => 'cat06_link',
		'type' => 'text',);


	// ------- Sessão Ebook ---------- //

	$options[] = array(
		'name' => __('Ebook', 'options_check'),
		'type' => 'heading');

	/* Box */

	$options[] = array(
		'name' => __('Titulo da chamada', 'options_check'),
		'id' => 'titulo_ebook',
		'type' => 'text');

	$options[] = array(
		'name' => __('Descrição da chamada', 'options_check'),
		'id' => 'desc_ebook',
		'type' => 'editor');

	$options[] = array(
		'name' => __('Título do botão', 'options_check'),
		'id' => 'titulo_botao',
		'type' => 'text');

	$options[] = array(
		'name' => __('Imagem do Ebook', 'options_check'),
		'id' => 'img_ebook',
		'type' => 'upload');

	


		// ------- Sessão Projetos ---------- //

	$options[] = array(
		'name' => __('Projetos', 'options_check'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Título da sessão', 'options_check'),
		'desc' => __('Projetos.', 'options_check'),
		'id' => 'projetos_titulo',
		'std' => 'Título da sessão',
		'type' => 'text');

	$options[] = array(
		'name' => __('Os elementos dessa sessão você consegue editar em Painel > Projetos', 'options_check')
		);

	// ------- Sessão Processo ---------- //

	$options[] = array(
		'name' => __('Processo', 'options_check'),
		'type' => 'heading');


		$options[] = array(
		'name' => __('Título da sessão', 'options_check'),
		'desc' => __('Processo.', 'options_check'),
		'id' => 'processo_titulo',
		'std' => 'Título da sessão',
		'type' => 'text');

		$options[] = array(
		'name' => __('Descrição', 'options_check'),
		'desc' => __('Descrição sobre o processo.', 'options_check'),
		'id' => 'processo_desc',
		'type' => 'textarea');

		// ------- Sessão Depoimentos ---------- //

	$options[] = array(
		'name' => __('Depoimentos', 'options_check'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Título da sessão', 'options_check'),
		'desc' => __('Quem Somos.', 'options_check'),
		'id' => 'quemsomos_titulo',
		'std' => 'Título da sessão',
		'type' => 'text');

	$options[] = array(
		'name' => __('Descrição Acessórios', 'options_check'),
		'desc' => __('Descrição sobre Acessórios.', 'options_check'),
		'id' => 'acessorios_descricao',
		'std' => 'Descrição sobre Acessórios',
		'type' => 'textarea');


// ------- Sessão Noticias ---------- //

	$options[] = array(
		'name' => __('Noticias', 'options_check'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Título da sessão', 'options_check'),
		'desc' => __('Notícias.', 'options_check'),
		'id' => 'noticias_titulo',
		'std' => 'Título da sessão',
		'type' => 'text');

	$options[] = array(
		'name' => __('Os artigos desta sessão você pode editar em Painel > Artigos', 'options_check')
		);

		// ------- Sessão Marcas ---------- //

	$options[] = array(
		'name' => __('Marcas', 'options_check'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Marca 01', 'options_check'),
		'desc' => __('Imagem da marca.', 'options_check'),
		'id' => 'marca_1',
		'type' => 'upload');

	$options[] = array(
		'name' => __('Marca 02', 'options_check'),
		'desc' => __('Imagem da marca.', 'options_check'),
		'id' => 'marca_2',
		'type' => 'upload');


	$options[] = array(
			'name' => __('Marca 03', 'options_check'),
			'desc' => __('Imagem da marca.', 'options_check'),
			'id' => 'marca_3',
			'type' => 'upload');


	$options[] = array(
			'name' => __('Marca 04', 'options_check'),
			'desc' => __('Imagem da marca.', 'options_check'),
			'id' => 'marca_4',
			'type' => 'upload');


	$options[] = array(
			'name' => __('Marca 05', 'options_check'),
			'desc' => __('Imagem da marca.', 'options_check'),
			'id' => 'marca_5',
			'type' => 'upload');


	return $options;
}